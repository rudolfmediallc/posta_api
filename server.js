const restify = require('./config/restify.cfg.js');
const DatabaseServer = require('./config/mongoose.cfg.js');
const GatewayRouter = require('./config/routes');
const SocketServer = require('./config/socket.cfg.js');
const server = restify.initializeRESTServer();

GatewayRouter.initializeRouter(server);
DatabaseServer.initializeDbConnection();
SocketServer.mountCourierSockets();





