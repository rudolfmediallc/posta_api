const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoPopulate = require('mongoose-autopopulate');

//Status:
// 0: Created/Awaiting Confirm
// 1: Confirmed
// 2: Courier In Route
// 3: Received
// 4: Delivered
// 5: Canceled

var tripSchema = new Schema({
    trip_id: String,
    status: Number,
    confirm_code: Number,
    instructions: { type: String, default: null },
    contacted: Boolean,
    description: String,
    pickup: {
        name: {
            first: { type: String, default: " " },
            last: { type: String, default: " " }
        },
        phone: { type: String, required: true },
        address: {
            street: { type: String, required: true },
            city: { type: String, default: 'Chicago' },
            state: { type: String, default: "IL" },
            zip: { type: String, required: true },
        },
        sender: {
            type: Schema.Types.ObjectId,
            ref: 'Sender',
        }
    },
    pickup_geo: {
        type: { type: String, default: 'Point', required: true},
        coordinates: [Number]
    },
    destination: {
        name: {
            first: { type: String, default: " " },
            last: { type: String, default: " " }
        },
        phone: { type: String, required: true },
        address: {
            street: { type: String, required: true },
            city: { type: String, default: "Chicago" },
            state: { type: String, default: "IL" },
            zip: { type: String, required: true },
        }
    },
    destination_geo: {
        type: { type: String, default: "Point", required: true},
        coordinates: [Number]
    },
    courier: {
        type: Schema.Types.ObjectId,
        ref: 'Courier',
        default: null,
        autopopulate: true
    },
    timestamps: {
        created: { type: Date, default: null },
        updated: { type: Date, default: null },
        confirmed: { type: Date, default: null },
        accepted: { type: Date, default: null },
        received: { type: Date, default: null },
        delivered: { type: Date, default: null }
    },
    transaction: {
        type: Schema.Types.ObjectId,
        ref: 'Transaction',
        autopopulate: true
    }
});

tripSchema.pre('save', function (next) {
    if(this.isModified('status')) {
        switch (this.status) {
        case 1:
            this.timestamps.confirmed = Date.now();
            break;
        case 2:
            this.timestamps.accepted = Date.now();
            break;
        case 3:
            this.timestamps.received = Date.now();
            break;
        case 4:
            this.timestamps.delivered = Date.now();
            break;
        }
    }
    this.timestamps.updated = Date.now();
    next();
});


//Indexing & Plugins
tripSchema.index({ status: 1, type: -1 });
tripSchema.plugin(autoPopulate);
tripSchema.index({ 'destination_geo': '2dsphere' });
tripSchema.index({ 'pickup_geo': '2dsphere' });
module.exports = mongoose.model('Trip', tripSchema);
