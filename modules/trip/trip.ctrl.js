'use strict';
const response = require('../../config/response');
const Trip = require('./trip');
const Sender = require('../sender/sender');
const User = require('../user/user');
const SMSController = require('../sms/sms.ctrl');
const DispatchController = require('../dispatch/dispatch.ctrl');
const DispatchScheduler = require('../dispatch/dispatch.schdlr');
const CourierController = require('../courier/courier.ctrl');
const helpers = require('../../config/helpers');
const TransactionController = require('../transaction/transaction.ctrl');

module.exports.getTripsForUser = getTripsForUser;
module.exports.postNewTrip = postNewTrip;
module.exports.updateTripStatus = updateTripStatus;
module.exports.index = index;


function index(req, res, next){
    let query = helpers.setQuery(req.params, ['sender', 'status']);
    Trip.findOne(query)
        .limit(20)
        .sort({ created_at: 1 })
        .exec()
        .then(trips => {
            return response.success(res, next, 200, trips, "trips");
        })
        .catch(err => {
            return response.failure(res, next, 500, err);
        });
}

function getTripsForUser(req, res, next){
    var query = {};
    if (req.query.courier){
        query = Trip.find({ courier : req.params.id});
        if (req.query.current == "true"){
            query.and([{$or: [{status: 2}, {status: 3}]}]);
        } else{
            query.and([{$or: [{status: 4}, {status: 5}]}]);
        }
    } else{
        query = Trip.find({'pickup.sender': req.params.id});
        if (req.query.current == "true"){
            query.and([{$or: [{status: 0}, {status: 1}, {status: 2}, {status: 3}]}]);
        } else{
            query.and([{$or: [{status: 4}, {status: 5}]}]);
        }
    }

    query.sort({ 'timestamps.created': -1 })
         .exec()
         .then(trips => {
             return response.success(res, next, 200, trips, "trips");
         })
         .catch(err => {
             return response.failure(res, next, 500, err);
         });
}

async function postNewTrip(req, res, next){
    var savedTrip = null;
    try {
        var senderUser = await getSender(req);
        var transaction = await createAndValidateTransaction(senderUser);
        savedTrip = await createTrip(req, senderUser.profile, transaction);
        await SMSController.contactRecipient(savedTrip);
        return response.success(res, next, 201, savedTrip, "trip");
    }
    catch(err) {
        console.log(err);
        if (savedTrip){
            savedTrip.remove();
        }
        return response.failure(res, next, 500, err);
    }
}

function getSender(req){
    return new Promise((resolve, reject) =>{
        User.Sender.findOne({ profile: req.body.sender_id })
            .populate('profile')
            .exec()
            .then(sender => {
                console.log("SENDER" + sender);
                if (sender === undefined){
                    return reject("Sender Not found");
                }
                return resolve(sender);
            })
            .catch(err => {
                return reject(err);
            });
    });
}

function createAndValidateTransaction(senderUser){
    return new Promise((resolve, reject) =>{
        TransactionController.createAuthCharge(senderUser)
                             .then(transaction => {
                                 console.log("TRANSACTION" + transaction);
                                 return resolve(transaction)
                             })
                             .catch(err => {
                                 return reject(err)
                             });
    });
}

function createTrip(req, sender, transaction){
    return new Promise((resolve, reject) =>{
        var newTrip = new Trip({
            status: 0,
            confirm_code: null,
            instructions: req.body.instructions,
            description: req.body.description,
            transaction: transaction,
            pickup: {
                name: {
                    first: sender.name.first,
                    last: sender.name.last,
                },
                phone: sender.phone,
                address: {
                    street: sender.address.street,
                    city: sender.address.city,
                    state: sender.address.state,
                    zip: sender.address.zip,
                },
                sender: sender
            },
            pickup_geo: {
                coordinates : [sender.location.coordinates[0], sender.location.coordinates[1]]
            },
            destination: {
                name: {
                    first: req.body.destination.name.first,
                    last: req.body.destination.name.last,
                },
                phone: req.body.destination.phone,
                address: {
                    street: req.body.destination.address.street,
                    city: req.body.destination.address.city,
                    state: req.body.destination.address.state,
                    zip: req.body.destination.address.zip,
                }
            },
            destination_geo: {
                coordinates : [req.body.destination_geo.coordinates[0], req.body.destination_geo.coordinates[1]]
            },
            timestamps: {
                created: Date.now()
            }
        });
        newTrip.trip_id = setTrackingID(5);
        newTrip.save()
               .then(trip => {
                   return resolve(trip)
               })
               .catch(err => {
                   return reject(err)
               });
    });
}

async function updateTripStatus(req, res, next){
    try {
        var trip = await getTripForStatusUpdate(req);
        var updated = await updateStatusForTrip(req, trip);
        var status = updated.status;
        switch (true){
        case (status == 1):
            DispatchScheduler.registerDispatchEventForTrip(trip.id);
            break;
        case (status == 3):
            SMSController.sendTrackingTorRecipient(trip);
            break;
        case (status == 4):
            console.log(trip);
            CourierController.removeOrderFromCourier(trip);
            await TransactionController.captureTripCharge(trip);
            break;
        case (status == 5 && trip.courier != null):
            CourierController.removeOrderFromCourier(trip);
            TransactionController.processCanceledTrip(trip);
            DispatchController.notifyCourierCanceled(trip);
            break;
        case (status == 5):
            TransactionController.processCanceledTrip(trip);
            break;
        default:
            break;
        }
        return response.success(res, next, 200, updated, "trip");
    } catch (err){
        console.log(err);
        return response.failure(res, next, 500, err);
    }
}

function getTripForStatusUpdate(req){
    return new Promise((resolve, reject) =>{
        Trip.findById({ _id: req.params.id })
            .populate('courier')
            .exec()
            .then(trip => {
                return resolve(trip)
            })
            .catch(err => {
                return reject(err)
            });
    });

}

function updateStatusForTrip(req, trip){
    console.log(req.body);
    return new Promise((resolve, reject) =>{
        switch (req.body.status){
        case 1:
            CourierController.removeOrderFromCourier(trip);
            trip.status = req.body.status;
            trip.courier = null;
            break;
        case 3:
            trip.status = req.body.status;
            break;
        case 4:
            trip.status = req.body.status;
            break;
        case 5:
            trip.status = req.body.status;
            break;
        default:
            return reject("Request Malformed")
        }
        trip.save()
            .then(updated => {
                return resolve(updated)
            })
            .catch(err => {
                return reject(err)
            });
    });
}

function setTrackingID(length){
    var id = 'TR-',
        charset = 'ABC1DEF2GH3UVW4XYZ5IJK6LMN7OP8QR9ST0';
    for(var i = 0; i < length; i++){
        id  += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    return id;
}
