const router = require('restify-router').Router;
const TripController = require('./trip.ctrl');
const tripRouter = new router();


/*
    V1 Routes
 */

tripRouter.get('/trips/:id', TripController.getTripsForUser);
tripRouter.get('/trips', TripController.index);
tripRouter.post('/trips', TripController.postNewTrip);
tripRouter.put('/trips/:id', TripController.updateTripStatus);

module.exports.router = tripRouter;
