const router = require('restify-router').Router;
const regionCtrl = require('./region.ctrl');
const regionRouter = new router();

regionRouter.post('/regions', regionCtrl.newRegion);
regionRouter.get('/regions', regionCtrl.getDeliveryRegion);

module.exports.router = regionRouter;