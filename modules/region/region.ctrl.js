'use strict';
const response = require('../../config/response');
const Region = require('./region');

module.exports.getDeliveryRegion = getDeliveryRegion;
module.exports.newRegion = newRegion;

function getDeliveryRegion(req, res, next){
    Region.findOne({})
          .sort({ created_at: -1 })
          .exec(function(err, trips){
              if (err) return response.failure(res, next, 500, err);
              return response.success(res, next, 200, trips, "region");
          });
}

function newRegion(req, res, next){
    var newRegion = new Region({
        bounds:{
            max_lat: req.body.max_lat,
            max_lon: req.body.max_lon,
            min_lat: req.body.min_lat,
            min_lon: req.body.min_lon,
        }
    });
    newRegion.save(function(err, region){
       if (err) return response.failure(res, next, 500, err);
        return response.success(res, next, 201, region, "region");
    });
}