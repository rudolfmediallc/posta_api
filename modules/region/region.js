const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var regionSchema = new Schema({
    bounds:{
        max_lat: { type: Number, default: null },
        max_lon: { type: Number, default: null },
        min_lat: { type: Number, default: null },
        min_lon: { type: Number, default: null }
    }
});

module.exports = mongoose.model("Region", regionSchema);
