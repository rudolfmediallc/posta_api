'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const deepPopulate = require('mongoose-deep-populate')(mongoose);
const courier = require('../courier/courier');
const sender = require('../sender/sender');
const bcrypt = require('bcrypt-node');

var options = { discriminatorKey: 'kind' };
var userSchema = new Schema({
        email: { type: String, required: true, index: { unique: true } },
        name: { first: String, last: String },
        phone: String,
        password_digest: { type: String, required: true, default: null },
        operative: { type: Boolean, required: true, default: true },
        device: {
            id: { type: String, default: null },
            type: { type: String, default: null },
            os: { type: String, default: null },
        },
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    }, options);

var courierUser = new Schema({
    address: {
        street: String,
        city: String,
        state: String,
        zip: String
    },
    stripe_account: {type: String, default: null },
    profile: {
        type: Schema.Types.ObjectId,
        ref: 'Courier'
    }
});

courierUser.pre('save', function(next){
    if (this.created) next();
    else {
        courier.update({ _id: this.profile._id },
            {
                $set: {
                    'name.first': this.name.first,
                    'name.last': this.name.last
                }
            }, function(err){
                if (err) console.log(err);
            });
        next();
    }
});

var senderUser = new Schema({
    profile: {
        type: Schema.Types.ObjectId,
        ref: 'Sender'
    },
    stripe_id: { type: String, default: null },
    default_payment: { type: String, default: null }
});

senderUser.pre('save', function(next){
    if (this.created) next();
    else {
        sender.update({ _id: this.profile._id },
            {
                $set: {
                    'name.first': this.name.first,
                    'name.last': this.name.last,
                    phone: this.phone
                }
            }, function(err){
                if (err) console.log(err);
            });
        next();
    }
});

courierUser.pre('save', function(next){
    if (this.created) next();
    else {
        courier.update({ _id: this.profile._id },
            {
                $set: {
                    'name.first': this.name.first,
                    'name.last': this.name.last,
                    phone: this.phone
                }
            }, function(err){
                if (err) console.log(err);
            });
        next();
    }
});

//var adminUser = new Schema({});
userSchema.pre('save', function(next){
    if (!this.isModified('password_digest')) return next();
    let self = this;
    bcrypt.hash(this.password_digest, null, null, function(err, hash){
        if (err) return next(err);
        self.password_digest = hash;
        next();
    });
});

userSchema.method('checkPassword', function(password, cb){
    bcrypt.compare(password, this.password_digest, function(err, match){
        if (err) return cb(err, false);
        cb(null, match);
    });
});

userSchema.plugin(deepPopulate);

var User = mongoose.model('User', userSchema);

module.exports.base = User;
//module.exports.Admin = User.discriminator('AdminUser', adminUser);
module.exports.Courier = User.discriminator("CourierUser", courierUser);
module.exports.Sender = User.discriminator("SenderUser", senderUser);

