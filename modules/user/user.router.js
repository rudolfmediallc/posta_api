const router = require('restify-router').Router;
const UserController = require('./user.ctrl');
const userRouter = new router();

/*
    V1 Routes
 */
userRouter.get('/users/:id', UserController.getUser);
userRouter.put('/users/:id', UserController.updateUser);
userRouter.post('/users/senders', UserController.createNewSender);
userRouter.post('/users/couriers', UserController.createNewCurier);
//userRouter.put('/user/:id', UserController.updatePasswordForUser);

module.exports.router = userRouter;
