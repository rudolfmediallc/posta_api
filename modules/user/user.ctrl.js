"use strict";
const response = require('../../config/response');
const User = require('./user');
const Courier = require('../courier/courier');
const Sender = require('../sender/sender');
const AuthController = require('../auth/auth.ctrl');

module.exports.createNewCurier = createNewCourier;
module.exports.createNewSender = createNewSender;
module.exports.updatePasswordForUser = updatePasswordForUser;
module.exports.getUser = getUser;
module.exports.updateUser = updateUser;

/*
 Queries
 */
//function getUser(req){
//    return new Promise((resolve, reject) =>{
//        User.base.findById(req.params.id)
//            .populate('profile')
//            .exec()
//            .then(user => {
//                return resolve(user)
//            })
//            .catch(err => {
//                return reject(err)
//            });
//    });
//}

/*
 Create New Courier User
 */
async function createNewCourier(req, res, next){
    try {
        var profile = await createCourierProfile(req);
        var user = await createCourierUser(req, profile);
        AuthController.issueNewToken(res, user);
        return response.success(res, next, 201, user, "user");
    } catch (err) {
        console.log(err);
        return response.failure(res, next, 500, err);
    }
}

function createCourierProfile(req){
    return new Promise((resolve, reject) =>{
        var newCourier = new Courier({
            name: {
                first: req.body.name.first,
                last: req.body.name.last
            },
            active: false,
            phone: req.body.phone,
        });
        newCourier.save()
                  .then(profile => {
                      return resolve(profile)
                  })
                  .catch(err => {
                      return reject(err)
                  });
    });
}

function createCourierUser(req, profile){
    return new Promise((resolve, reject) =>{
        var newCourier = new User.Courier({
                email: req.body.email,
                password_digest: req.body.password,
                name: {
                    first: req.body.name.first,
                    last: req.body.name.last
                },
                phone: req.body.phone,
                address: {
                    street: req.body.address.street,
                    city: req.body.address.city,
                    state: req.body.address.state,
                    zip: req.body.address.zip
                },
                profile: profile
            }
        );
        newCourier.save()
                  .then(courierUser => {
                      return resolve(courierUser)
                  })
                  .catch(err => {
                      profile.remove();
                      return reject(err)
                  });
    });
}

/*
 Create New Venue
 */
async function createNewSender(req, res, next){
    try {
        var profile = await createSenderProfile(req);
        var user = await createSenderUser(req, profile);
        AuthController.issueNewToken(res, user);
        return response.success(res, next, 201, user, "user");
    } catch (err) {
        console.log(err);
        return response.failure(res, next, 500, err);
    }
}

function createSenderProfile(req){
    return new Promise((resolve, reject) =>{
        var senderProfile = new Sender({
            name: {
                first: req.body.name.first,
                last: req.body.name.last
            },
            phone: req.body.phone,
        });
        senderProfile.save()
                     .then(sender => {
                         return resolve(sender)
                     })
                     .catch(err => {
                         return reject(err)
                     });

    });
}

function createSenderUser(req, profile){
    return new Promise((resolve, reject) =>{
        var newUser = new User.Sender({
            email: req.body.email,
            password_digest: req.body.password,
            name: {
                first: req.body.name.first,
                last: req.body.name.last
            },
            phone: req.body.phone,
            profile: profile
        });
        newUser.save()
               .then(senderUser => {
                   return resolve(senderUser)
               })
               .catch(err => {
                   profile.remove();
                   return reject(err)
               });
    });
}

/*
 Update Password
 */
async function updatePasswordForUser(req, res, next){
    try {
        var user = await getUser(req);
        await checkPassword(req, user);
        var updated = await updatePassword(req, user);
        return response.success(res, next, 200, updated, "user");
    } catch (err) {
        console.log(err);
        return response.failure(res, next, 500, err);
    }
}

function getUser(req){
    return new Promise((resolve, reject) =>{
        User.base.findById(req.params.id)
            .populate('profile')
            .exec()
            .then(user => {
                return resolve(user)
            })
            .catch(err => {
                return reject(err)
            });
    });
}

function checkPassword(req, user){
    return new Promise((resolve, reject) =>{
        user.checkPassword(req.body.password, function(err, match){
            user.password_digest = undefined;
            if (match) return resolve(match);
            else return reject("Current Password does not match records.")
        });
    });
}

function updatePassword(req, user){
    return new Promise((resolve, reject) =>{
        user.password_digest = req.body.password_digest;
        user.save()
            .then(user => {
                return resolve(user)
            })
            .catch(err => {
                return reject(err)
            });
    });
}

/*
 Update User Info
 */

async function updateUser(req, res, next){
    try {
        var user = await getUser(req);
        var updated = await updateAndSave(req, user);
        return response.success(res, next, 200, updated, "user");
    } catch (err){
        console.log(err);
        return response.failure(res, next, 500, err);
    }
}

function updateAndSave(req, user){
    return new Promise((resolve, reject) =>{
        console.log(req.body);
        try {
            switch (req.body.type) {
            case 'name':
                user = updateName(req, user);
                break;
            case 'email':
                user = updateEmail(req, user);
                break;
            case 'phone':
                user = updatePhone(req, user);
                break;
            case 'password':
                break;
            case 'address':
                user = updateAddress(req, user);
                break;
            default:
                return reject("Could not update profile.");
            }
        } catch(err){
            console.log(err);
            return reject("Could not update profile.");
        }
        user.save()
            .then(user => {
                return resolve(user);
            }).catch(err => {
            return reject(err);
        });
    });
}

function updateName(req, user){
    user.name.first = req.body.first || user.name.first;
    user.name.last = req.body.last || user.name.last;
    return user
}

function updateEmail(req, user){
    user.email = req.body.email || user.email;
    return user
}

function updatePhone(req, user){
    user.phone = req.body.phone || user.phone;
    return user
}

function updateAddress(req, user){
    user.address.street = req.body.street;
    user.address.city = req.body.city;
    user.address.state = req.body.state;
    user.address.zip = req.body.zip;
    return user
}

