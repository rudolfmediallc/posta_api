const router = require('restify-router').Router;
const StripeController = require('./stripe.ctrl');
const stripeRouter = new router();

/*
    V1 Routes
 */

stripeRouter.get('/stripe/account/:id', StripeController.getAccountLoginLink);
stripeRouter.get('/stripe/connect_courier', StripeController.saveStripeConnectIdForCourier);
stripeRouter.get('/stripe/cards', StripeController.getCard);
stripeRouter.post('/stripe/payment', StripeController.updatePayment);

module.exports.router = stripeRouter;