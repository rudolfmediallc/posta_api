const response = require('../../config/response');
const Stripe = require('../../config/stripe.cfg').Client;
const User = require('../user/user');
const PriceController = require('../transaction/price.ctrl');
const STRIPE_KEYS = require('../../config/constants').STRIPE;
var curl = require('curlrequest');

module.exports.getAccountLoginLink = getAccountLoginLink;
module.exports.saveStripeConnectIdForCourier = saveStripeConnectIdForCourier;
module.exports.updatePayment = updatePayment;
module.exports.getCard = getCard;
module.exports.getDefaultPayment = getDefaultPayment;
module.exports.validateCharge = validateCharge;
module.exports.captureAuthCharge = captureAuthCharge;
module.exports.createTransfer = createTransfer;
module.exports.captureAuthChargeForCanceledTrip = captureAuthChargeForCanceledTrip;
module.exports.createTransferForCancledTrip = createTransferForCanceledTrip;
module.exports.cancelAuthCharge = cancelAuthCharge;

/*
    (Courier Only)
    Get Login Link for Connect Users
 */

async function getAccountLoginLink(req, res, next){
    try{
        var acctID = await getAccountId(req);
        var link = await getLink(acctID);
        return response.success(res, next, 200, link, "link");
    }
    catch (err){
        return response.failure(res, next, 500, err);
    }
}

function getAccountId(req){
    return new Promise((resolve, reject) =>{
        User.Courier.findById(req.params.id, function(err, user){
            if (err) return reject(err);
            return resolve(user.stripe_account);
        });
    });
}

function getLink(accountId){
    return new Promise((resolve, reject) =>{
        Stripe.accounts.createLoginLink(accountId, function(err, link){
            if (err) return reject(err);
            return resolve(link.url);
        });
    });
}

//
//async function createStripeCustomer(req, res, next){
//    try{
//        var customer = await createCustomer(req);
//        var u = await getLink(acctID);
//        return response.success(res, next, 200, link, "link");
//    }
//    catch (err){
//        return response.failure(res, next, 500, err);
//    }
//}
//
//function getAccountId(req){
//    return new Promise((resolve, reject) =>{
//        User.Courier.findById(req.params.id, function(err, user){
//            if (err) return reject(err);
//            return resolve(user.stripe_account);
//        });
//    });
//}
//
//function getLink(accountId){
//    return new Promise((resolve, reject) =>{
//        Stripe.accounts.createLoginLink(accountId, function(err, link){
//            if (err) return reject(err);
//            return resolve(link.url);
//        });
//    });
//}

/*
    (Courier Only)
    Attach Stripe Connect Account to Courier Profile
 */

async function saveStripeConnectIdForCourier(req, res, next){
    try{
        const code = req.query.code;
        var user = await getUser(req);
        var stripeResponse = await finalizeAccount(code);
        await saveStripeAccountId(stripeResponse, user);
        return res.send(200);
    }
    catch (err){
        console.log(err);
        return response.failure(res, next, 500, err);
    }
}

function getUser(req){
    return new Promise((resolve, reject) =>{
        if (!req.query.state) return reject("User Not Found");
        User.Courier.findById(req.query.state, function(err, user){
            if (err) return reject(err);
            return resolve(user);
        });
    });
}

function finalizeAccount(code){
    return new Promise((resolve, reject) =>{
        var options = {
            url: "https://connect.stripe.com/oauth/token",
            method: 'POST',
            data : {
                'client_secret': STRIPE_KEYS.TEST_KEY,
                'code': code,
                'grant_type': 'authorization_code',
            }
        };
        curl.request(options, function(err, res){
            if (err) return reject(err);
            return resolve(JSON.parse(res));
        });
    });
}

function saveStripeAccountId(stripeResponse, user){
    return new Promise((resolve, reject) =>{
        user.stripe_account = stripeResponse.stripe_user_id;
        user.save()
            .then(user => {
                return resolve(user)
            })
            .catch(err => {
                return reject(err)
            });
    });
}

/*
    (Sender Only)
    Exchange Payment Method Temporary Token for Source Object.
    Remove Current Source Object if there is one.
    Attach Source Object to Sender User
 */

async function updatePayment(req, res, next){
    try{
        var user = await getStripeCustomer(req);
        var card = await exchangeSourceToken(req);
        if (user.default_payment != null){
            await removePastToken(req, user);
        }
        var updated = await savePaymentToken(card, user);
        return response.success(res, next, 200, updated, "user");
    }
    catch (err){
        console.log(err);
        return response.failure(res, next, 500, err);
    }
}

function getStripeCustomer(req){
    return new Promise((resolve, reject) =>{
        User.Sender.findOne({ stripe_id: req.body.stripe_id }).populate('profile')
            .exec()
            .then(user => {
                return resolve(user)
            })
            .catch(err => {
                return reject(err)
            });
    });
}

function exchangeSourceToken(req){
    return new Promise((resolve, reject) =>{
        Stripe.customers.createSource( req.body.stripe_id, { source: req.body.default_payment })
              .then(res => {
                  console.log(res);
                  return resolve(res)
              })
              .catch(err => {
                  return reject(err)
              });
    });
}

function removePastToken(req, user){
    return new Promise((resolve, reject) =>{
        Stripe.customers.deleteCard(req.body.stripe_id, user.default_payment)
              .then(del => {
                  return resolve(del)
              })
              .catch(e => {
                  return reject(e)
              });
    });
}

function savePaymentToken(card, user){
    return new Promise((resolve, reject) =>{
        user.default_payment = card.id;
        user.save()
            .then(user => {
                return resolve(user)
            })
            .catch(e => {
                return reject(e)
            });
    });
}

/*
    (Sender Only)
    Get Current Payment Method
 */

function getCard(req, res, next){
    Stripe.customers.retrieveCard(
        req.query.stripe_id,
        req.query.default_payment)
          .then(card => {
              return response.success(res, next, 200, card, "card")
          })
          .catch(err => {
              return response.failure(res, next, 500, err)
          });
}

/*
    (For Transaction Creation)
    Get Current Payment Method
 */

function getDefaultPayment(user){
    return new Promise((resolve, reject) =>{
        Stripe.customers.retrieveCard(
            user.stripe_id,
            user.default_payment)
              .then(card => {
                  return resolve(card)
              })
              .catch(() => {
                  return reject("Could not retrieve payment. Please ensure your payment method info is up to date.")
              });
    });
}

function validateCharge(user, card){
    return new Promise((resolve, reject) =>{
        Stripe.charges.create({
            amount: PriceController.amount.total_charge,
            currency: 'usd',
            description: 'Posta, Inc. Delivery Fee',
            source: card.id,
            customer: user.stripe_id,
            capture: false,
        })
              .then(authCharge => {
                  return resolve(authCharge)
              })
              .catch(() => {
                  return reject("Could not authorize payment. Please ensure your payment method info is up to date.")
              });
    });
}

/*
    Capture Authorization Charge Complete Trip
 */

function captureAuthCharge(txn){
    return new Promise((resolve, reject) => {
        Stripe.charges.capture(
            txn.charge.auth_charge
        ).then(res => {
            return resolve(res)
        }).catch(err => {
            return reject(err)
        });
    });
}

function createTransfer(txn, destinationAccount, captureRes){
    return new Promise((resolve, reject) => {
        Stripe.transfers.create({
            amount: txn.amount.destination_amount,
            currency: "usd",
            destination: destinationAccount,
            source_transaction: captureRes.id,
        }).then(res => {
            return resolve(res)
        }).catch(err => {
            return reject(err)
        });
    });
}


/*
    Capture Authorization Charge Cancled Trip
 */

function captureAuthChargeForCanceledTrip(txn){
    return new Promise((resolve, reject) => {
        Stripe.charges.capture(
            txn.charge.auth_charge,
            {
                amount: txn.amount.total_charge
            }
        ).then(res => {
            return resolve(res)
        }).catch(err => {
            return reject(err)
        });
    });
}


function createTransferForCanceledTrip(txn, destinationAccount, captureRes){
    return new Promise((resolve, reject) => {
        Stripe.transfers.create({
            amount: txn.amount.destination_amount,
            currency: "usd",
            destination: destinationAccount,
            source_transaction: captureRes.id,
        }).then(res => {
            return resolve(res)
        }).catch(err => {
            return reject(err)
        });
    });
}

function cancelAuthCharge(txn){
    return new Promise((resolve, reject) => {
        Stripe.refunds.create({
            charge: txn.charge.auth_charge
        }).then(res => {
            return resolve(res)
        }).catch(err => {
            return reject(err)
        });
    });
}