'use strict';
const restify  = require('restify');
const jwt = require('jsonwebtoken');
const response = require('../../config/response');
const User = require('../user/user');

const SECRET = "DERRP";

exports.authenticateAllUsers = function(req, res, next){
  if(req.headers['authorization'] && req.headers['authorization'].startsWith('JWT')){
    const token = req.headers['authorization'].substr(4);
    jwt.verify(token, SECRET, function(err, decoded){
        if(err) return unauthorized(next);
        User.base.findById(decoded.user._id, function(err, user){
            if(err || !user) return unauthorized(next);
            if (!user.operative) return response.failure(res, next, 401, 'Access Revoked');
            req.user = user;
           return next();
        });
    });
  } else{
      return unauthorized(next);
  }
};

exports.authenticateAdmin = function(req, res, next){
    if(req.headers['authorization'] && req.headers['authorization'].startsWith('JWT')){
        const token = req.headers['authorization'].substr(4);
        jwt.verify(token, SECRET, function(err, decoded){
            if(err) return unauthorized(next);
            User.base.findById(decoded.user._id, function(err, user){
                if(err || !user) return unauthorized(next);
                if (user.__t != "AdminUser") return unauthorized(next);
                req.user = user;
                return next();
            });
        });
    } else{
        return unauthorized(next);
    }
};

exports.setToken = function(user){
  return jwt.sign({user: user.id}, SECRET, { expiresIn: '90d' });
};

function unauthorized(next){
    return next(new restify.UnauthorizedError('Unauthorized request'));
}