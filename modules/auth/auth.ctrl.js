"use strict";
const response = require('../../config/response');
const User = require('../user/user');
const JWT = require('jsonwebtoken');
const AUTH = require('../../config/constants').JWT;

module.exports.issueNewToken = issueNewToken;

module.exports.signInSender = function(req, res, next){
    if (!req.body.email || !req.body.password){
        return response.failure(res, next, 400, 'Invalid Login Credentials');
    }
   User.Sender.findOne({email: req.body.email}).populate('profile').exec( function(err, user){
       if (err) return response.failure(res, next, 500, err);
       if (!user) return response.failure(res, next, 401, 'Username Invalid');
       if (!user.operative) return response.failure(res, next, 401, 'Access revoked, account inactive.');
       user.checkPassword(req.body.password, function(err, match){
           user.password_digest = undefined;
           if(match){
               issueNewToken(res, user);
               return response.success(res, next, 201, user, "user");
           }else{
               console.log(err);
               return response.failure(res, next, 401, 'Password Incorrect');
           }
       });
    });
};

module.exports.signInCourier = function(req, res, next){
    if (!req.body.email || !req.body.password){
        return response.failure(res, next, 400, 'Invalid Login Credentials');
    }
    User.Courier.findOne({email: req.body.email}).populate('profile').exec( function(err, user){
        if (err) return response.failure(res, next, 500, err);
        if (!user) return response.failure(res, next, 401, 'Password Incorrect');
        if (!user.operative) return response.failure(res, next, 401, 'Access revoked, account inactive.');
        user.checkPassword(req.body.password, function(err, match){
            user.password_digest = undefined;
            if(match){
                issueNewToken(res, user);
                return response.success(res, next, 201, user, "user");
            }else{
                console.log(err);
                return response.failure(res, next, 401, 'Password Incorrect');
            }
        });
    });
};

function issueNewToken(res, user){
    var token = JWT.sign({user: user.id}, AUTH.JWT_SECRET, { expiresIn: '5d' });
    return res.setHeader('authorization', token);
};