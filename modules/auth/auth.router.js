const router = require('restify-router').Router,
      authCtrl = require('./auth.ctrl'),
      authRouter = new router();

authRouter.post('/auth/sender', authCtrl.signInSender);
authRouter.post('/auth/courier', authCtrl.signInCourier);

module.exports.router = authRouter;
