
const helpers = require('../../config/helpers');
const Trip = require('../trip/trip');
const DispatchScheduler = require('../dispatch/dispatch.schdlr');
const Twilio = require('../../config/twilio.config').Client;
const MessagingResponse = require('../../config/twilio.config').MessagingResponse;
//const contactNumber = "13123130108";
const contactNumber = "13129577622";
const RECHECK_NUMBER = "Error contacting recipient, please double check the phone number and try again.";

module.exports.contactRecipient = contactRecipient;
module.exports.sendTrackingTorRecipient = sendTrackingTorRecipient;
module.exports.handleIncomingMessage = handleIncomingMessage;

/*
    Contact Recipient
*/

async function contactRecipient(trip){
    try {
        await contactRecipientInitial(trip);
        await contactRecipientForAddress(trip);
        return trip
    }
    catch(err) {
        throw new Error(err);
    }
}

function contactRecipientInitial(trip){
    return new Promise((resolve, reject) =>{
        const message = {
            body: getInitialMessageString(trip),
            from: contactNumber,
            to: '+1' + trip.destination.phone
        };
        Twilio.messages.create(message)
              .then(response => {
                  return resolve(response)
              })
              .catch( e => {
                  console.log(e);
                  return reject(RECHECK_NUMBER)
              });
    });
}

function contactRecipientForAddress(trip){
    return new Promise((resolve, reject) =>{
        const message = {
            body: getAddressMessageString(trip),
            from: contactNumber,
            to: '+1' + trip.destination.phone
        };
        Twilio.messages.create(message)
              .then(response => {
                  return resolve(response)
              })
              .catch(err => {
                  console.log(err);
                  return reject(RECHECK_NUMBER)
              });
    });
}

function sendTrackingTorRecipient(trip){
    return new Promise((resolve, reject) =>{
        const message = {
            body: getRecipientTrackingString(trip),
            from: contactNumber,
            to: '+1' + trip.destination.phone
        };
        Twilio.messages.create(message)
              .then(response => {
                  return resolve(response)
              })
              .catch(err => {
                  console.log(err);
                  return reject(RECHECK_NUMBER)
              });
    });
}


/*
     WebHooks Receiving From Twilio / Client Response
*/

async function handleIncomingMessage(req, res, next){
    try {
        var trip = await getTripForRecipient(req.body);
        var confirmed = await updateDeliveryStatus(req.body.Body);
        var updatedTrip = await updateTrip(trip, confirmed);
        if (confirmed === false) await sendAddressDeclinedAck(updatedTrip);
        else await sendConfirmCode(updatedTrip);
        DispatchScheduler.registerDispatchEventForTrip(updatedTrip.id);

        const twilioRes = new MessagingResponse();
        res.writeHead(200, {'Content-Type': 'text/xml'});
        res.end(twilioRes.toString());

        return next(false);
    }
    catch(err) {
        console.log(err);
    }
}

function getTripForRecipient(body){
    return new Promise((resolve, reject) =>{
        Trip.findOne({ 'destination.phone' : formatPhoneNumber(body.From)})
            .and({ status : 0 })
            .exec()
            .then(trip => {
                if (trip == null){
                    return reject("Trip Not Found");
                }
                return resolve(trip);
            })
            .catch(err => {
                return reject(err);
            });
    });
}

function updateDeliveryStatus(smsRes){
    return new Promise((resolve, reject) =>{
        const confirmed = reduceAndTrimString(smsRes);
        switch (confirmed) {
        case "YES":
            return resolve(true);
        case "NO":
            return resolve(false);
        default:
            return reject("SMS response malformed")
        }
    });
}

function updateTrip(trip, confirmed){
    return new Promise((resolve, reject) =>{
        console.log(trip);
        if (confirmed) {
            trip.status = 1;
            trip.confirm_code = generateConfirmCode(4);
        } else {
            trip.status = 6;
        }
        trip.save()
            .then(trip => {
                console.log("trip updated" + trip);
                return resolve(trip)
            })
            .catch(err => {
                return reject(err)
            });
    });
}

function sendAddressDeclinedAck(updatedTrip){
    return new Promise((resolve) =>{
        var senderMessage = {
            body: getSenderAddressDeclinedString(updatedTrip),
            from: contactNumber,
            to: '+1' + updatedTrip.pickup.phone
        };
        var recipientMessage = {
            body: getRecipientAddressDeclinedString(updatedTrip),
            from: contactNumber,
            to: '+1' + updatedTrip.destination.phone
        };
        Twilio.messages.create(senderMessage)
              .then(message => console.log(message.sid))
              .done();
        Twilio.messages.create(recipientMessage)
              .then(message => console.log(message.sid))
              .done();
        return resolve(updatedTrip);
    });
}

function sendConfirmCode(updatedTrip){
    return new Promise((resolve, reject) =>{
        const message = {
            body: getConfirmCodeMessage(updatedTrip),
            from: contactNumber,
            to: '+1' + updatedTrip.destination.phone
        };
        Twilio.messages.create(message)
              .then(response => {
                  return resolve(response)
              })
              .catch(err => {
                  return reject(RECHECK_NUMBER)
              });
    });
}

/*
    String Helpers
 */

function generateConfirmCode(length){
    var id = '';
    var charset = '1234567890';
    for(var i = 0; i < length; i++){
        id  += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    return id;
}

function reduceAndTrimString(str){
    var trim = str.trim();
    return trim.toUpperCase();
}

function getConfirmCodeMessage(trip){
    return `[Posta]: You're all set! Your courier will need your confirmation code upon receipt to complete your delivery. Your code is: ${trip.confirm_code} `
}

function getRecipientAddressDeclinedString(trip){
    const senderName = capitalize(trip.pickup.name.first) + ' ' + capitalize(trip.pickup.name.last);
    return `[Posta]: Delivery canceled. ${senderName} has been notified of the incorrect address.`
}

function getRecipientTrackingString(trip){
    const tripNumber = trip.trip_id;
    return `[Posta]: Your courier is on the way! Track their progress at http://www.posta.tech/tracking/${tripNumber}.`
}

function getSenderAddressDeclinedString(trip){
    const recipientName = capitalize(trip.destination.name.first) + ' ' + capitalize(trip.destination.name.last);
    return `[Posta]: ${recipientName} has declined the receiving address. Please get in touch to re-confirm the receiving address.`
}

function getInitialMessageString(trip){
    const senderName = capitalize(trip.destination.name.first);
    const recipientName = capitalize(trip.pickup.name.first) + ' ' + capitalize(trip.pickup.name.last);
    return `[Posta]: Hi, ${senderName} you have a new delivery request from ${recipientName}. Please confirm your receiving address:`
}

function getAddressMessageString(trip){
    const street = capitalize(trip.destination.address.street);
    const city = capitalize(trip.destination.address.city);
    const state = trip.destination.address.state;
    const zip = trip.destination.address.zip;
    return `${street}\n${city}, ${state} ${zip}\n\nReply 'YES' or 'NO'`
}

function capitalize(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function formatPhoneNumber(string){
    return string.slice(2);
}