const router = require('restify-router').Router;
const SMSController = require('./sms.ctrl');
const SMSRouter = new router();

/*
    V1 Routes
 */

SMSRouter.post('/sms', SMSController.handleIncomingMessage);

module.exports.router = SMSRouter;