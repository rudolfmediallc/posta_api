const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var senderSchema = new Schema({
    name: {
        first: { type: String, default: "" },
        last: { type: String, default: "" }
    },
    phone: { type: String, required: true },
    address: {
        street: { type: String, required: true, default: " " },
        city: { type: String, default: "Chicago" },
        state: { type: String, default: "IL" },
        zip: { type: String, required: true, default: " " },
    },
    location: {
        type: { type: String, default: "Point", required: true},
        coordinates: { type: [Number], default: [0,0] }
    }
});

senderSchema.index({ "location": "2dsphere" });
module.exports = mongoose.model("Sender", senderSchema);
