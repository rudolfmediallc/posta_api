'use strict';
const response = require('../../config/response');
const Sender = require('../sender/sender');
const helpers = require('../../config/helpers');

module.exports.updateSenderLocation = updateSenderLocation;

async function updateSenderLocation(req, res, next){
    try {
        var sender = await getSender(req);
        var updated = await updateLocation(sender, req);
        return response.success(res, next, 201, updated, "sender");
    }
    catch(err) {
        console.log(err);
        return response.failure(res, next, 500, err);
    }
}

function getSender(req){
    return new Promise((resolve, reject) =>{
        Sender.findById(req.body._id).exec()
              .then(sender => {
                  if (!sender){
                      return reject("Sender not found")
                  }
                  return resolve(sender)
              })
              .catch(err => {
                  return reject(err)
              });
    });
}

function updateLocation(sender, req){
    return new Promise((resolve, reject) =>{
        sender.address.street = req.body.address.street;
        sender.address.city = req.body.address.city;
        sender.address.zip = req.body.address.zip;
        sender.address.state = req.body.address.state;
        sender.location.coordinates =  req.body.location.coordinates;
        sender.save()
              .then(updated => {
                  return resolve(updated);
              })
              .catch(err => {
                  return reject(err)
              });
    });
}

