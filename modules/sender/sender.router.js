const router = require('restify-router').Router;
const SenderController = require('./sender.ctrl');
const senderRouter = new router();

/*
    V1 Routes
 */
senderRouter.put('/sender', SenderController.updateSenderLocation);

module.exports.router = senderRouter;