const SocketSync = require('./socket.sync');

/*
    Sync Incoming Socket State
 */

module.exports.setCourierSyncController = function(io, courierSockets){
    courierSockets.on('connection', function(socket){
        console.log('Courier Connected');

        socket.on('logOutCourier', function(){
            SocketSync.removeCourierSocket(socket.id);
            socket.disconnect();
        });

        socket.on("disconnect", function(){
            SocketSync.removeCourierSocket(socket.id);
            console.log("Disconnect Called");
        });

        socket.on('registerSocket', function(data){
            if (data) {
                console.log("Registering Socket");
                SocketSync.setCourierSocket(data, socket, function(shouldProceed){
                    if (shouldProceed == true) socket.emit("startUpdatingLocation", {"proceed" : true});
                    else socket.disconnect()
                });
            }
        });

        socket.on('publishLocationUpdate', function(data){
            if (data) SocketSync.publishLocationUpdate(data);
        });
    });
};
