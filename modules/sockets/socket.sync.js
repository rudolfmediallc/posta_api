"use strict";
const Courier = require('../courier/courier');
const DispatchController = require('../dispatch/dispatch.ctrl');

const openCourierSockets = [];

module.exports.setCourierSocket = setCourierSocket;
module.exports.removeCourierSocket = removeCourierSocket;
module.exports.publishLocationUpdate = publishLocationUpdate;
module.exports.openCourierSockets = openCourierSockets;

/*
    Sync Incoming Socket with Courier State
 */

function setCourierSocket(req, socket, callback){
    if (!req.id) {
        return callback(false)
    }
    var courier = {
        id: req.id,
        socket_id: socket.id,
    };
    openCourierSockets.push(courier);
    loginCourier(courier.id);
    console.log("Socket Added " + socket.id);
    return callback(true)
}

function removeCourierSocket(socketId){
    for (var i = 0, len = openCourierSockets.length; i < len; ++i) {
        var openSocket = openCourierSockets[i];
        if (!openSocket) return;
        if (openSocket.socket_id == socketId) {
            openCourierSockets.splice(i, 1);
            console.log("Socket Removed: " + socketId);
            logoutCourier(openSocket.id);
            DispatchController.removeFromContactingByCourier(openSocket.id)
        }
    }
}

function publishLocationUpdate(data){
    if (data.id && data.lat && data.lon) {
        Courier.findByIdAndUpdate(data.id, { $set: { 'location.coordinates': [data.lon, data.lat] } }, { new: true })
               .exec()
               .catch(err => {
                   console.log("Error Publishing Location Update." + err);
               });
    }
}

function logoutCourier(courier_id){
    Courier.findByIdAndUpdate(courier_id, { $set: { active: false } })
           .exec()
           .then(updated => {
               console.log(("Courier Logged Out" + updated.id));
           })
           .catch(err => {
               return console.log("Error Logging Out Courier" + courier_id + " --- " + err);
           });
}

function loginCourier(courier_id){
    Courier.findByIdAndUpdate(courier_id, { $set: { active: true } })
           .exec()
           .then(updated => {
               console.log(("Courier Logged In" + updated.id));
           })
           .catch(err => {
               return console.log("Error Logging In Courier" + courier_id + " --- " + err);
           });
}