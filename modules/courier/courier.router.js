const router = require('restify-router').Router;
const CourierController = require('./courier.ctrl');
const courierRouter = new router();

/*
    V1 Routes
 */
courierRouter.get('/couriers', CourierController.getCourierLocations);

module.exports.router = courierRouter;