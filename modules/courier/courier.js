const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var courierSchema = new Schema({
    name: {
        first: { type: String, default: "" },
        last: { type: String, default: "" }
    },
    phone: String,
    active: Boolean,
    transport_mode: { type: Number, default: 0 },
    current_trip: {
        type: Schema.Types.ObjectId,
        ref: 'Trip',
        default: null
    },
    location: {
        type: { type: String, default: "Point", required: true},
        coordinates: { type: [Number], default: [0,0] }
    },
});

courierSchema.index({ 'location': '2dsphere' });
module.exports = mongoose.model("Courier", courierSchema);
