const response = require('../../config/response');
const Courier = require('../courier/courier');

module.exports.getCourierLocations = getCourierLocations;

/*
    (Sender Only)
    Get Online Couriers and locations
 */

module.exports.removeOrderFromCourier = removeOrderFromCourier

function getCourierLocations(req, res, next){
    console.log(req.query.lon);
    console.log(req.query.lon);
    Courier.find({ location: { $near: { $maxDistance: 2000, $geometry: { type: "Point", coordinates: [req.query.lon, req.query.lat] } } }}, 'location.coordinates')
           .and({ active: true })
           .exec()
           .then(locations => response.success(res, next, 200, locations, "locations"))
           .catch(err => response.failure(res, next, 500, err));
}

function removeOrderFromCourier(trip){
    return new Promise((resolve, reject) =>{
        Courier.findById(trip.courier, function(err, courier){
            if (courier) {
                courier.current_trip = null;
                courier.save()
            }
            return resolve(true)
        });
    });
}