const response = require('../../config/response');
const phoneFormatter = require('libphonenumber-js');
const Trip = require('../trip/trip');

module.exports.getTrackingInfo = getTrackingInfo;

function getTrackingInfo(req, res, next){
    Trip.findOne({trip_id: req.params.id}).populate('courier')
        .exec().then(trip => {
            return response.success(res, next, 200, reduceResponse(trip), "trip")
        })
        .catch(err => {
            return response.failure(res, next, 500, err)
        });
}

function reduceResponse(trip){
    return {
        destination_first: trip.destination.name.first,
        destination_last: trip.destination.name.last,
        destination_street: trip.destination.address.street,
        destination_city_zip: trip.destination.address.city + " " + trip.pickup.address.zip,
        destination_coordinates: trip.destination_geo.coordinates,

        courier_first: trip.courier.name.first,
        courier_last: trip.courier.name.last,
        courier_phone: phoneFormatter.formatNumber({ country: 'US', phone: trip.courier.phone }, 'National'),
        courier_coordinates: trip.courier.location.coordinates,

        received: trip.timestamps.received,
        delivered: trip.timestamps.delivered,
        status: getStatus(trip.status),
        trip_id: trip.trip_id,
    }
}

function getStatus(status){
    switch (status){
    case 3: return "Out For Delivery";
    case 4: return "Delivered";
    default: return "Unknown"
    }
}