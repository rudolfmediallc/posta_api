const Router = require('restify-router').Router;
const TrackingController = require('../tracking/tracking.ctrl');
const trackingRouter = new Router();

/*
    V1 Routes
 */
trackingRouter.get('/tracking/:id', TrackingController.getTrackingInfo);

module.exports.router = trackingRouter;