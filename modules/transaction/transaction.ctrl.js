'use strict';
const response = require('../../config/response');
const Transaction = require('./transaction');
const User = require('../user/user');
const PriceController = require('./price.ctrl');
const StripeController = require('../stripe/stripe.ctrl');


// should refactor to store a the order short ID on the capture charge

module.exports.createAuthCharge = createAuthCharge;
module.exports.captureTripCharge = captureTripCharge;
module.exports.processCanceledTrip = processCanceledTrip;

/*
    Create Auth Charge for new Trip
 */

async function createAuthCharge(user){
    try {
        var card = await StripeController.getDefaultPayment(user);
        var chargeResponse = await StripeController.validateCharge(user, card);
        return await createTransactionWithCharge(user, chargeResponse, card);
    } catch (err) {
        return err;
    }
}

function createTransactionWithCharge(user, charge, card, transferGroup){
    return new Promise((resolve, reject) =>{
        var transaction = new Transaction({
            transfer_group: transferGroup,
            sender: {
                id: user.profile.id,
                stripe_id: user.stripe_id,
                source_id: user.default_payment
            },
            charge: {
                auth_charge: charge.id,
                capture_charge: null,
                destination_charge: null,
                card_type: card.brand,
                last4: card.last4,
            },
            amount: PriceController.amount,
        });
        transaction.timestamps.authorized = Date.now();
        transaction.save()
                   .then(transaction => {
                       return resolve(transaction)
                   })
                   .catch(e => {
                       return reject(e)
                   });
    });
}

/*
    Capture Auth Charge for Completed Trip
 */

async function captureTripCharge(trip){
    try{
        var txn = await getTransactionForTrip(trip);
        var destinationAccount = await getDestinationAccount(trip.courier);
        var captureRes = await StripeController.captureAuthCharge(txn);
        var transfer = await StripeController.createTransfer(txn, destinationAccount, captureRes);
        await updateTransactionDetails(txn, trip, destinationAccount, captureRes, transfer);
    }
    catch(err){
        console.log(err);
    }
}

function getTransactionForTrip(trip){
    return new Promise((resolve, reject) =>{
        Transaction.findById(trip.transaction)
                   .exec()
                   .then(txn => {
                       return resolve(txn)
                   })
                   .catch(err => {
                       return reject(err)
                   });
    });
}

function getDestinationAccount(courier){
    return new Promise((resolve, reject) =>{
        User.Courier.findOne({profile: courier})
            .exec()
            .then(courier => {
                return resolve(courier.stripe_account)
            })
            .catch(err => {
                return reject(err)
            });
    });
}

function updateTransactionDetails(txn, trip, destinationAccount, captureRes, transfer){
    return new Promise((resolve, reject) =>{
        txn.charge.capture_charge = captureRes.id;
        txn.courier.id = trip.courier.id;
        txn.courier.stripe_account = destinationAccount;
        txn.timestamps.captured = Date.now();
        txn.charge.transfer_id = transfer.id;
        txn.transfer_group = transfer.transfer_group;
        txn.trip = trip;
        txn.save()
           .then(t => {
               return resolve(t);
           })
           .catch(err => {
               return reject(err);
           });
    });
}

/*
    Cancel Transaction
 */

async function processCanceledTrip(trip){
    try {
        var txn = await getTransactionForTrip(trip);
        var adjusted = await getAdjustedTransaction(txn, trip);
        console.log(adjusted);
        if (adjusted.amount.total_charge == 0){
            StripeController.cancelAuthCharge(txn);
            console.log("CAHHED PROPRER ")
        } else {
            console.log("CAHHED wrong ")
            var destinationAccount = await getDestinationAccount(trip.courier);
            var captureRes = await StripeController.captureAuthChargeForCanceledTrip(txn);
            var transfer = await StripeController.createTransferForCancledTrip(txn, destinationAccount, captureRes);
            await updateTransactionDetails(txn, trip, destinationAccount, captureRes, transfer);
        }
    }
    catch(err){
        console.log(err)
    }
}

function getAdjustedTransaction(txn, trip){
    return new Promise((resolve, reject) =>{
        switch (true) {
        case (trip.courier == null):
            txn.amount = PriceController.canceledBeforeCourier;
            break;
        case (trip.courier != null):
            txn.amount = PriceController.canceledWithCourier;
            break;
        default:
            return reject("Could not update Transaction");
            break;
        }
        txn.save()
           .then(t => {
               return resolve(t);
           })
           .catch(err => {
               return reject(err);
           });
    });
}

/*
    Handle auth charges on stripe for Canceled order
 */
