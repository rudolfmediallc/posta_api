const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var transactionSchema= new Schema({
    transfer_group: { type: String, default: "123" },
    sender: {
        id: { type: String, default: null },
        stripe_id: { type: String, default: null },
        source_id: {type: String, default: null }
    },
    courier: {
        id: { type: String, default: null },
        stripe_account: { type: String, default: null },
    },
    charge: {
        auth_charge: { type: String, default: null },
        capture_charge: { type: String, default: null },
        transfer_id: { type: String, default: null },
        card_type: { type: String, default: null },
        last4: { type: String, default: null },
    },
    amount: {
        total_charge: { type: Number, default: 0 },
        total_fees: { type: Number, default: 0 },
        destination_amount: { type: Number, default: 0 },
        withheld: { type: Number, default: 0 }
    },
    trip: {
        type: Schema.Types.ObjectId,
        ref: 'Trip',
        default: null
    },
    timestamps: {
        created: { type: Date, default: Date.now() },
        updated: { type: Date, default: Date.now() },
        authorized: { type: Date, default: null },
        captured: { type: Date, default: null },
    }
});

module.exports = mongoose.model("Transaction", transactionSchema);