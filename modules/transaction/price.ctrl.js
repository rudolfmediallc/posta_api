const amount = {
    total_charge: 2000,
    total_fees: 275,
    withheld: 500,
    destination_amount: 1225,
};

const canceledWithCourier = {
    total_charge: 500,
    total_fees: 0,
    withheld: 0,
    destination_amount: 500,
};
const canceledBeforeCourier = {
    total_charge: 0,
    total_fees: 0,
    withheld: 0,
    destination_amount: 0,
};

module.exports.amount = amount;
module.exports.canceledWithCourier = canceledWithCourier;
module.exports.canceledBeforeCourier = canceledBeforeCourier;