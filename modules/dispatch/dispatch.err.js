var trip_id;
var err_type;
var courier;

module.exports.DispatchError = DispatchError;

function DispatchError(trip_id, err_type){
    this.trip_id = trip_id;
    this.err_type = err_type;
    this.courier = null
}