const Trip = require('../trip/trip');
const Courier = require('../courier/courier');
const SocketSync = require('../sockets/socket.sync');
const SocketServer = require('./../../config/socket.cfg.js');
const DispatchScheduler = require('./dispatch.schdlr');
const DispatchError = require('./dispatch.err').DispatchError;

const NOT_CONNECTED = "SOCKET_NOT_CONNECTED";
const NO_COURIERS_AVAILABLE = "NO COURIERS AVAILABLE";
const SOCKET_TIMEOUT = "SOCKET_TIMEOUT";
const TRIP_DECLINED = "TRIP_DECLINED";

const contacting = [];

module.exports.startDispatchForTrip = startDispatchForTrip;
module.exports.notifyCourierCanceled = notifyCourierCanceled;
module.exports.removeFromContactingByCourier = removeFromContactingByCourier;

/*
    Find and Dispatch Closest available Courier
 */


async function startDispatchForTrip(dispatchEvent){
    console.log("Beginning Dispatch for trip... "  + dispatchEvent.trip_id);
    try {
        var trip = await getTrip(dispatchEvent.trip_id);
        var courier = await getAvailableCourier(trip, dispatchEvent);
        await addToProcessingList(courier.id, trip.id);
        await contactCourierOnSocket(trip, courier);
        var processed = await attachCourierToTrip(trip, courier);
        await attachTripToCourier(trip, courier);
        await notifyCourierTripAssigned(processed, courier);
        DispatchScheduler.removeDispatchEvent(dispatchEvent);
        console.log("Dispatch Complete for trip..." + dispatchEvent.trip_id)
    } catch(dispatchErr){
        console.log("Dispatch Failed with err..." + dispatchErr.err_type);
        removeFromContactingList(dispatchEvent.trip_id);
        DispatchScheduler.rescheduleDispatchEvent(dispatchEvent, dispatchErr)
    }
}
function getTrip(tripId){
    return new Promise((resolve, reject) =>{
        Trip.findById(tripId).exec()
            .then(trip => {
                return resolve(trip)
            })
            .catch(err => {
                var dispatchError = new DispatchError(tripId, err);
                return reject(dispatchError)
            });
    });
}

function getAvailableCourier(trip, dispatchEvent){
    return new Promise((resolve, reject) =>{
        Courier.findOne({ active: true })
               .and({ current_trip: null })
               .and({ location: { $near: { $maxDistance: 3000, $geometry: { type: "Point", coordinates: trip.pickup_geo.coordinates} } } }, 'location.coordinates')
               .and({ _id: { $not: { $in: getInProgressIds() } } })
               .and({ _id: { $not: { $in: dispatchEvent.declined } } })
               .exec()
               .then(courier =>{
                   if (!courier) {
                       var dispatchError = new DispatchError(trip.id, NO_COURIERS_AVAILABLE);
                       return reject(dispatchError)
                   }
                   return resolve(courier)
               })
               .catch(err => {
                   var dispatchError = new DispatchError(trip.id, err);
                   return reject(dispatchError)
               });
    });
}

function contactCourierOnSocket(trip, courier){
    return new Promise((resolve, reject) =>{
        var courierId = courier.id;
        var socket = SocketSync.openCourierSockets.filter(function(courierSockets){
            return courierSockets.id == courierId;
        })[0];
        if (!socket){
            var dispatchError = new DispatchError(trip.id, NOT_CONNECTED);
            return reject(dispatchError)
        }
        let timeout = setTimeout(() =>{
            var dispatchError = new DispatchError(trip.id, SOCKET_TIMEOUT);
            return reject(dispatchError)
        }, 60000);
        SocketServer.courierEngine().nsp.connected[socket.socket_id].emit('notifyTrip', JSON.stringify(trip), function(ack){
            clearTimeout(timeout);
            if (ack.accepted == true){
                return resolve(true);
            }
            if (ack.accepted == false){
                var dispatchError = new DispatchError(trip.id, TRIP_DECLINED);
                dispatchError.courier = courierId;
                return reject(dispatchError)
            }
        });
    });
}

function attachTripToCourier(trip, courier){
    return new Promise((resolve, reject) =>{
        courier.current_trip = trip.id;
        courier.save()
               .then(updated => {
                   return resolve(updated)
               })
               .catch(err => {
                   var dispatchError = new DispatchError(trip.id, err);
                   return reject(dispatchError)
               });
    });
}

function attachCourierToTrip(trip, courier){
    return new Promise((resolve, reject) =>{
        trip.courier = courier;
        trip.status = 2;
        trip.save()
            .then(updated => {
                return resolve(updated)
            })
            .catch(err => {
                var dispatchError = new DispatchError(trip.id, err);
                return reject(dispatchError)
            });
    });
}

function notifyCourierTripAssigned(trip, courier){
    return new Promise((resolve) =>{
        var courierId = courier.id;
        var socket = SocketSync.openCourierSockets.filter(function(courierSockets){
            return courierSockets.id == courierId;
        })[0];
        if (!socket){
            return resolve(null);
        }
        SocketServer.courierEngine().nsp.connected[socket.socket_id].emit('pushTrip', JSON.stringify(trip));
        return resolve(null)
    });
}


/*
     Cancel Trip
 */

function notifyCourierCanceled(trip){
    return new Promise((resolve, reject) =>{
        var courierId = trip.courier.id;
        var socket = SocketSync.openCourierSockets.filter(function(courierSockets){
            return courierSockets.id == courierId;
        })[0];
        if (!socket){
            console.log("Courier Not Connected Canceling... "  + trip.id);
            return reject(NOT_CONNECTED);
        }
        let timeout = setTimeout(() =>{
            console.log("Socket Timeout Called Canceling... "  + trip.id);
            return reject(SOCKET_TIMEOUT);
        }, 60000);
        SocketServer.courierEngine().nsp.connected[socket.socket_id].emit('cancelTrip', JSON.stringify(trip), function(ack){
            clearTimeout(timeout);
            if (ack) return resolve(true)
        });
    });
}


/*
    Sync Contacting Queue State
 */

function addToProcessingList(courier_id, trip_id){
    return new Promise((resolve) =>{
        var contact = {
            trip: trip_id,
            courier: courier_id
        };
        contacting.push(contact);
        return resolve(true);
    });
}


function removeFromContactingList(trip_id){
    for (var i = 0, len = contacting.length; i < len; ++i) {
        var contact = contacting[i];
        if (!contact.trip) return;
        if (contact.trip == trip_id) {
            contacting.splice(i, 1);
        }
    }
}

function getInProgressIds(){
    var list = [];
    for (var i = 0, len = contacting.length; i < len; ++i) {
        var contact = contacting[i];
        list.push(contact.courier);
    }
    return list
}

function removeFromContactingByCourier(courier_id){
    for (var i = 0, len = contacting.length; i < len; ++i) {
        var contact = contacting[i];
        if (!contact.courier) return;
        if (contact.courier == courier_id) {
            contacting.splice(i, 1);
        }
    }
}
