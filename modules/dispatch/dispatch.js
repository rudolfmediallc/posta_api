var trip_id;
var event;
var declined;

module.exports.DispatchEvent = DispatchEvent;

function DispatchEvent(trip_id){
    this.trip_id = trip_id;
    this.event = null;
    this.declined = [];
}

DispatchEvent.prototype.clearDeclined = function(){
    this.declined = []
};
