const Trip = require('../trip/trip');
const moment = require('moment');
const EventScheduler = require('node-schedule');
const DispatchController = require('./dispatch.ctrl');
const DispatchEvent = require('./dispatch').DispatchEvent;

const dispatchEvents = [];

const NOT_CONNECTED = "SOCKET_NOT_CONNECTED";
const NO_COURIERS_AVAILABLE = "NO COURIERS AVAILABLE";
const SOCKET_TIMEOUT = "SOCKET_TIMEOUT";
const TRIP_DECLINED = "TRIP_DECLINED";
const NEW_TRIP = "NEW_TRIP";

module.exports.registerDispatchEventForTrip = registerDispatchEventForTrip;
module.exports.rescheduleDispatchEvent = rescheduleDispatchEvent;
module.exports.populateDispatchQueue = populateDispatchQueue;
module.exports.removeDispatchEvent = removeDispatchEvent;
module.exports.NEW_TRIP = NEW_TRIP;


function registerDispatchEventForTrip(trip_id){
    var event = new DispatchEvent(trip_id);
    var triggerTime = getDispatchTime(NEW_TRIP);
    dispatchEvents.push(event);
    scheduleDispatchTaskOnThread(event, triggerTime);
}

function scheduleDispatchTaskOnThread(dispatchEvent, triggerTime){
    dispatchEvent.event = EventScheduler.scheduleJob(triggerTime, function(dispatchEvent){
        DispatchController.startDispatchForTrip(dispatchEvent);
    }.bind(null, dispatchEvent));
    console.log("Dispatch: Scheduled on Process " + dispatchEvent.trip_id);
}

function removeDispatchEvent(event){
    for (var i = 0, len = dispatchEvents.length; i < len; ++i) {
        var dispatchEvent = dispatchEvents[i];
        if (dispatchEvents.trip_id == event.trip_id) {
            dispatchEvents.splice(i, 1);
            dispatchEvent.event.cancel();
            console.log("Process Event Removed for Order " + event.trip_id);
            break;
        }
    }
}

function getDispatchTime(eventType){
    switch (eventType) {
    case NEW_TRIP:{
        return moment().add(5, 's').toDate();
    }
    case NOT_CONNECTED: {
        return moment().add(15, 's').toDate();
    }
    case NO_COURIERS_AVAILABLE: {
        return moment().add(15, 's').toDate();
    }
    case SOCKET_TIMEOUT: {
        return moment().add(15, 's').toDate();
    }
    case TRIP_DECLINED: {
        return moment().add(15, 's').toDate();
    }
    default:
        return moment().add(30, 's').toDate();
    }
}

function rescheduleDispatchEvent(dispatchEvent, dispatchError){
    switch (dispatchError.err_type) {
    case NO_COURIERS_AVAILABLE: {
        dispatchEvent.clearDeclined();
        break
    }
    case TRIP_DECLINED: {
        dispatchEvent.declined.push(dispatchError.courier);
        break
    }
    default:
        break
    }
    scheduleDispatchTaskOnThread(dispatchEvent, getDispatchTime(dispatchError.err_type));
}

function populateDispatchQueue(){
    Trip.find({status : 1})
        .sort({'timestamps.created' : -1})
        .exec(function(err, trips){
            if (err) return;
            var interval = 0;
            trips.forEach(function(trip){
                interval = interval + 5;
                var staggered = moment().add(interval, 's').toDate();
                var event = new DispatchEvent(trip.id);
                dispatchEvents.push(event);
                scheduleDispatchTaskOnThread(event, staggered);
            });
            console.log("Trips queued: " + trips.length);
        });
}