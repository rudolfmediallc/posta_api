'use strict';
module.exports.setQuery = function(params, allowed){
    let whitelist = allowed || [],
        query = {};

    if(params && whitelist.length){
        Object.keys(params).forEach(function(key){ 
            if(whitelist.indexOf(key) == -1) return;
            // if is _id AND id valid
            if(key.indexOf('_id') > -1 && module.exports.validID(params[key])){
              let k = key.replace('_id', '');
              query[k] = params[key];
              return false; 
            }
            query[key] = params[key];    
        })
    }
    return query;
};

module.exports.flattenJSON = function(data) {
    let flattened = {};
    function recursion (cur, prop) {
        if (Object(cur) !== cur) {
            flattened[prop] = cur;
        } else if (Array.isArray(cur)) {
             for(let i = 0, l = cur.length; i < l; i++)
                 recursion(cur[i], prop + "[" + i + "]");
            if (l == 0)
                flattened[prop] = [];
        } else {
            let empty = true;
            for (let p in cur) {
                empty = false;
                recursion(cur[p], prop ? prop + "." + p : p);
            }
            if (empty && prop)
                flattened[prop] = {};
        }
    }
    recursion(data, "");
    return flattened;
};

const hexRegExp = new RegExp("^[0-9a-fA-F]{24}$");

module.exports.validID = function(id){

  if(id == null) return false;
  if(typeof id == 'number') return true;
  if(typeof id == 'string') {
    return id.length == 12 || (id.length == 24 && hexRegExp.test(id));
  }
  return false;
};

/*
    EX: driver.ctrl.js: 

    function getById(req.params.id, function(err, driver){
        if(!helpers.validID(req.params.id)){
            return response.failure(res, next, 422, 'Invalid ID');
        }
        ...
    });
*/

