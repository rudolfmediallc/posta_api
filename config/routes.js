const courierRoutes = require('../modules/courier/courier.router');
const senderRoutes = require('../modules/sender/sender.router');
const tripRoutes = require('../modules/trip/trip.router');
const userRoutes = require('../modules/user/user.router');
const authRoutes = require('../modules/auth/auth.router');
const smsRoutes = require('../modules/sms/sms.router');
const regionRoutes = require('../modules/region/region.router');
const stripeRoutes = require('../modules/stripe/stripe.router');
const transactionRoutes = require('../modules/transaction/transaction.router');
const trackingRoutes = require('../modules/tracking/tracking.router');


module.exports.initializeRouter = function (server) {
        courierRoutes.router.applyRoutes(server);
        senderRoutes.router.applyRoutes(server);
        tripRoutes.router.applyRoutes(server);
        userRoutes.router.applyRoutes(server);
        authRoutes.router.applyRoutes(server);
        smsRoutes.router.applyRoutes(server);
        regionRoutes.router.applyRoutes(server);
        stripeRoutes.router.applyRoutes(server);
        transactionRoutes.router.applyRoutes(server);
        trackingRoutes.router.applyRoutes(server);
};
