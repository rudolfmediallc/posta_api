const Logger = require('bunyan');
const RotatingFileStream = require('bunyan-rotating-file-stream');

function reqSerializer(req){
    return {
        method: req.method,
        url: req.url,
        headers: req.headers
    };
}

var errorLogger = null;
var infoLogger = null;

module.exports.getLogger = function getLogger(){
    if (!errorLogger || errorLogger === null) {
        errorLogger = new Logger({
            name: 'deliveree_api',
            serializers: {
                req: reqSerializer
            },
            streams: [{
                level: 'error',
                stream: new RotatingFileStream({
                    path: './logs/error/api_errors.%b_%d.json',
                    period: '1d',
                    totalFiles: 15,
                    rotateExisting: true,
                    threshold: '10m',
                    totalSize: '20m',
                })
            }]
        });
    }

    if (!infoLogger || infoLogger === null) {
        infoLogger = new Logger({
            name: 'deliveree_api',
            serializers: {
                req: reqSerializer
            },
            streams: [{
                level: 'info',
                stream: new RotatingFileStream({
                    path: './logs/info/api_info.%b_%d.json',
                    period: '1d',
                    totalFiles: 15,
                    rotateExisting: true,
                    threshold: '10m',
                    totalSize: '20m',
                })
            }]
        });
    }

    function errLog(input){
        errorLogger.error(input);
    }

    function infoLog(input){
        infoLogger.info(input);
    }

    return {
        errLog: errLog,
        infoLog: infoLog
    }
};

//module.exports.errLog = function errorLogger(entry){
//    errorLogger.error(entry)
//};

//var infoLogger = new Logger({
//    name: 'deliveree_api',
//    serializers: {
//        req: reqSerializer
//    },
//    streams: [{
//        level: 'info',
//        stream: new RotatingFileStream({
//            path: './logs/info/api_info.%b_%d.json',
//            period: '1d',
//            totalFiles: 15,
//            rotateExisting: true,
//            threshold: '10m',
//            totalSize: '20m',
//        })
//    }]
//});
//
//
//module.exports.errLog = errorLogger.log();