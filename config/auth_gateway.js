'use strict';
const jwt = require('jsonwebtoken');
const response = require('./response');
const User = require('../modules/user/user');
const AUTH = require('./constants').JWT;

exports.AuthService = function(req, res, next){
    if (req.route.path.includes('/auth')) return next();
    if (req.route.path.includes('/sms')) return next();
    if (req.route.path.includes('/tracking')) return next();
    console.log(req.headers);
    if (req.headers['authorization'] && req.headers['authorization']){
        const token = req.headers['authorization'];
        jwt.verify(token, AUTH.JWT_SECRET, function(err, decoded){
            if(err) return response.unauthorized(res, next);
            User.base.findById(decoded.user, function(err, user){
                if(err || !user) return response.unauthorized(res, next);
                req.user = user;
                next();
            });
        });
    } else{
        return response.unauthorized(res, next);
    }
};
