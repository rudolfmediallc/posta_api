"use strict";
const restify = require('restify');
const gateway = require('./auth_gateway');
const sysLogger = require('./logger.cfg');
const response = require('./response');

const cors = require('restify-cors-middleware')({
    origins: ['*'],
    allowHeaders: ['authorization'],
    exposeHeaders: ['authorization']
});

const settings = {
    port: 3000,
};

module.exports.initializeRESTServer = function () {
    var server = restify.createServer({
        name:'posta-rest-process',
        formatters: {
            'application/hal+json': restify.formatters['application/json; q=0.4']
        }
    });

    server.pre(cors.preflight);
    server.use(cors.actual);
    server.use(restify.plugins.bodyParser());
    server.use(restify.plugins.queryParser());
    //server.use(gateway.AuthService);
    server.listen(settings.port, function() {
        console.log('REST Client: Listening on Port ', settings.port);
    });
    server.on('uncaughtException', function (req, res, route, err) {
        console.log('REST Client: UncaughtException', err.stack);
    });
    return server
};
