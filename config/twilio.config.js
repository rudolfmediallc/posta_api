const TWILIO = require('./constants').TWILIO;
const twilio = require('twilio')(TWILIO.ACCT_SID, TWILIO.AUTH_TOKEN);
const msgResponse = require('twilio').twiml.MessagingResponse;

module.exports.Client = twilio;
module.exports.MessagingResponse = msgResponse;