"use strict";
const mongoose = require('mongoose');
const DispatchScheduler = require('../modules/dispatch/dispatch.schdlr');
const MONGOOSE = require('../config/constants').MONGOOSE;

exports.initializeDbConnection = function(){
    mongoose.Promise = global.Promise;
    mongoose.connect(MONGOOSE.DB_URL).then( d => {
            console.log("Atlas DB Client: Connection Established");
            DispatchScheduler.populateDispatchQueue();
            return mongoose.connection;
        },
        err => {
            console.log("Atlas DB Client: " + err);
        }
    );
};