const STRIPE = require('./constants').STRIPE;
const stripe = require('stripe')(STRIPE.TEST_KEY);
module.exports.Client = stripe;
