function respond(res, next, success, data, http_code, error, collectionName) {
    var response = {
        'success' : success,
        'error' : error
    };
    response[collectionName] = data;
    res.writeHead(http_code, {'content-type': 'application/vnd.api+json'});
    res.end(JSON.stringify(response));
    return next();
}

module.exports.unauthorized = function(res, next) {
    var err = new Error();
    err.message = "Unauthorized";
    err.code = 401;
    var response = {
        'success' : false,
        'error' : err
    };
    res.writeHead(401, {'content-type': 'application/vnd.api+json'});
    res.end(JSON.stringify(response));
    return next(false);
};

module.exports.success = function(res, next, http_code, data, collectionName) {
    respond(res, next, true, data, http_code, null, collectionName);
};

module.exports.failure = function(res, next, http_code, details){
    var e = new Error();
    if (typeof details == "string"){
        e.code = http_code;
        e.message = details;
    }
    else {
        if (details.name) {
            switch (details.name) {
                case "ValidationError":
                    e.message = details.message;
                    e.code = 400;
                    http_code = 400;
                    break;

                case "CastError":
                    e.message = details.message;
                    e.code = 400;
                    http_code = 400;
                    break;
                case "MongoError":
                    if (details.code && details.code == 11000){
                        e.message = "This User Is Already Registered";
                        e.code = 400;
                        http_code = 409;
                    }
                    break;

                default:
                    e.message = details.message;
                    e.code = http_code;
                    break;
            }
        }
    }
    respond(res, next, false, null, http_code, e);
};
