const SocketController = require('./../modules/sockets/socket.ctrl.js');
const http = require('http');

module.exports.mountCourierSockets = mountCourierSockets;
module.exports.courierEngine = getCourierEngine;

var courierEngine = {
    ioServer: undefined,
    nsp: undefined,
    port: 3001
};

function getCourierEngine() {
    return courierEngine
}

function mountCourierSockets() {
    var server = http.createServer().listen(courierEngine.port);
    courierEngine.ioServer = require('socket.io').listen(server);
    courierEngine.ioServer.reconnects = true;
    courierEngine.nsp = courierEngine.ioServer.of('/courier-sockets');
    SocketController.setCourierSyncController(courierEngine.ioServer, courierEngine.nsp);
    console.log("Socket Client: Serving Courier Engine on Port " + courierEngine.port + " @ " + courierEngine.nsp.name);
    return courierEngine.ioServer;
}
